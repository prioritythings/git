# GIT cheat Sheet

### Configuration

```bash
git config -h
git config --global --list
git config --list
```

### Repository

```bash
git init
git remote -h
git remote -v
git remote add origin [git-repo-url]
git remote set-url origin [git-repo-url]
git remote add [name] git@github.com:example/reponame.git
git remote set-url --push [name] DISABLE
git branch --set-upstream-to=origin/<branch> <branch>

git branch -h
git branch --all
git checkout -b [new_branch]
git branch [new_branch]
```

### Push

```bash
git push -h
```

#### Simple : Push all Changes

```bash
git add .
git reset HEAD # undo changes
git commit -am "Add all"
git push
```

#### Simple : Push empty directory

```bash
mkdir emptydirname
cd emptydirname
touch .keep
```

#### Simple : Ignore

```bash
touch .gitignore
echo "/.idea" >> .gitignore # jetbrains editor
```

#### Simple : Push some changes

#### Rebase

#### Conflict

### Trouble Shoot

**1.Cài đặt CRLF cho git trên window**


![core.autocrlf](images/git-001.png)

![core.autocrlf](images/git-002.png)

- Ref : http://paginaswebpublicidad.com/questions/4895/git-tren-windows-cai-dat-crlf-co-nghia-la-gi

> bạn nên sử dụng core.autocrlf = input trên tất cả các nền tảng. Trong trường hợp này nếu Git phải đối mặt CRLF nó sẽ ngầm biến nó thành LF và các tệp hiện có với LF vẫn như cũ.

### Atom Config Terminal

- Install: https://atom.io/packages/platformio-ide-terminal
- Terminal Config: C:\Program Files\Git\bin\bash.exe
- MarkDown : https://atom.io/packages/markdown-writer
- LineEnding: https://www.developmentsindigital.com/posts/2016-08-27-setting-atom-default-line-endings/
